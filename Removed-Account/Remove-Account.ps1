<#
.SYNOPSIS   
Script to removed user accounts from the domain with a long input list
    
.DESCRIPTION 
The script will use a plaintext file containing usernames to resolve accounts and remove them from the domain
	
.PARAMETER InputFile
A path that contains a plaintext file with account names

.PARAMETER LogPath
Set custom path for logfile, will default to C:\temp if no input

.NOTES   
Name: Remove-Account
Author: Justin Church
Version: 1.0
DateCreated: 2016-07-15
DateUpdated: 

.EXAMPLE
.\Remove-Account.ps1 -InputFile "C:\path_to_text_file.txt"

.DESCRIPTION
Will read usernames from specified path and output logfile to C:\temp

.EXAMPLE
.\Remove-Account.ps1 -InputFile "C:\path_to_text_file.txt" -LogPath "C:\path_to_log_output.log"

.DESCRIPTION
Will read usernames from specified path and output logfile to designated file
#>


Begin {

    Param(
    [Parameter(Mandatory=$true,HelpMessage="Path to file containing users")]
    [string]$Script:inputfile=$(throw "-InputFile is required."),
    [string]$Script:LogPath="c:\temp\RemoveUserlog$((Get-Date).ToString('yy-MM-dd')).log"
    )

    Function DateStamp {
        return "$(Get-Date -UFormat %Y%m%d-%H%M%S):"
    }

    Function LogEntry([string]$LogData) {
        Add-Content $LogPath "$(DateStamp) $LogData"
    }
    LogEntry "Importing User list and removing accounts"
    $Script:users = Get-Content $inputfile
    $Script:Failed = $null
    $Script:i = 0
    
}

Process {
    Foreach ($user in $users) {
        Write-Progress -Activity "Removing Accounts" -CurrentOperation "Attempting to remove $user" -Id 1 -PercentComplete (($i/$users.count)*100) -Status "$($i+1) of $($users.count)"
        try {
            LogEntry "Removing account for $user"
            $user | Get-ADUser | Remove-ADUser -Verbose
            Write-Host -ForegroundColor Green "Removed account for $user"
            $i++
        } catch {
            Write-Host -ForegroundColor Red "Failed to resolve or remove account $user"
            LogEntry "Failed to resolve or remove account $user"
            $Failed += $user
            $i++
            continue
        }
    }
}

End {
    LogEntry $Failed
    Write-Host "Removal complete, review $logpath for any failures" -ForegroundColor Yellow
}