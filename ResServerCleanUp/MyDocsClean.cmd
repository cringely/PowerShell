@echo off
setlocal EnableDelayedExpansion

rem Get date info
set DAY=%DATE:~7,2%
set YEAR=%DATE:~10,4%
set HOUR=%time:~0,2%
set MONTH=%DATE:~4,2%
set LOGFILE1="%~dp0Logs\%YEAR%.%MONTH%.%DAY%.%~n0.log"
MD "%~dp0Logs"

rem Request user profile path
set /p profile="Local path to profiles: "

rem Get folder listing and save to file for call
echo. > c:\temp\doclisting.txt
dir %profile% /a:d /b>> c:\temp\doclisting.txt
cd /d %profile%

rem Assume folder names match usernames and attempt locate in AD
for /f "skip=1 tokens=* delims= " %%a in (c:\temp\doclisting.txt) do (
echo %%a
dsquery user -samid %%a|find /v ""

rem If no user found, break share and rename folder
if errorlevel 1 (
echo %%a Not Found in %userdomain%, renaming folder
ren %%a _%%a
echo %%a Not found in AD, breaking %profile%\%%a$ share and renaming to _%%a >> %LOGFILE1%

rem If matching samid found, ignore folder
) else (
echo %%a Found in %userdomain%, ignoring folder
)
)

rem Remove file containing directory listing
del c:\temp\doclisting.txt
echo Logfile of changes is located at %LOGFILE1%

ENDLOCAL
pause