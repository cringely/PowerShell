$servers = Get-Content C:\TEMP\import.txt
if (!(test-path c:\temp\DebugLogs)) {mkdir C:\temp\DebugLogs}
foreach ($server in $servers) {
    If (Test-Path ("\\"+$server+"\c$\Windows\MEMORY.DMP")) {
        $dumppath = ("\\"+$server+"\c$\Windows")
        Write-Host ("\\"+$server+"\c$\Windows\MEMORY.DMP") -ForegroundColor Green
        Start-Process -wait -FilePath 'C:\Program Files\Debugging Tools for Windows (x64)\kd.exe' -ArgumentList "-logo c:\temp\DebugLogs\$server.txt -y `"SRV*C:\Symbols`" -i $dumppath*D:\Sources*$dumppath\system32 -z $dumppath\MEMORY.DMP -c `"!analyze -v;q`""
        }
    Elseif (Test-Connection -ComputerName $server -Count 2 -Quiet) {Write-Host "$server Memory Dump file doesn't seem to exist, but $server is pingable!" -ForegroundColor Yellow}
    Else {Write-Host "$server is unreachable" -ForegroundColor Red}
}