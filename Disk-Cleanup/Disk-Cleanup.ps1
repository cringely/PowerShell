<#----------------------------------------------------------------------------
Original Author: Tom Moser, PFE
Modified by: Justin Church, CSRA

Usage: .\Cleanup-Disk.Ps1 [-NoReboot] [-LogPath <String>]

Switch: NoReboot - Specify this switch ONLY if you DO NOT want the server to reboot
        post update. It is recommendend that you do NOT use this switch.

        LogPath - Specify this parameter with a log location to write out the script log.
                  Will default to log.txt in the script directory.

Notes: In order to schedule the script successfully, the name must remain Cleanup-Disk.ps1.
       The log file will contain all relevent information - no console output should be expected. 

Summary:
    This script requires KB2852386.

    The script itself will perform the following:
        -Update registry keys for cleanmgr.exe to run. 
        -Run cleanmgr.exe 
        -Exit 

-----------------------------------------------------------------------------#>

Param([string]$LogPath="c:\temp\cleanmgrlog.txt",      
      [switch]$NoReboot=$true)
if (Test-Path $LogPath) {Remove-Item $LogPath}
if (Test-Path c:\temp\DiskCleanupTranscript.log) {Remove-Item c:\temp\DiskCleanupTranscript.log}

Start-Transcript -Force -Path c:\temp\DiskCleanupTranscript.log

#Reg Paths/Vars
Set-Variable -Name ScriptRegKey -Value "HKLM:\Software\WinSXSCleanup" -Option Constant
if (Test-Path $ScriptRegKey) {Remove-Item $ScriptRegKey}
Set-Variable -Name ScriptRegValueName -Value "Phase" -Option Constant
Set-Variable -Name ScriptSageValueName -Value "SageSet" -Option Constant
Set-Variable -Name SageSet -Value "0010" -Option Constant
Set-Variable -Name ScriptSpaceBeforeValue -Value "SpaceBefore" -Option Constant
Set-Variable -Name SchTaskName -Value "CleanMgr Task Cleanup" -Option Constant
Set-Variable -Name ScriptDEStatusatStart -Value "DEInstalledAtStart" -Option Constant
Set-Variable -Name ScriptInkStatusAtStart -Value "InkInstalledAtStart" -Option Constant
Set-Variable -Name UpdateCleanupPath -value "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VolumeCaches\Update Cleanup" -Option Constant
Set-Variable -Name ServicePackCleanupPath -Value "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VolumeCaches\Service Pack Cleanup" -Option Constant
Set-Variable -Name VolumeCachesPath -Value "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VolumeCaches" -Option Constant
Set-Variable -Name StateFlagClean -Value 2 -Option Constant
Set-Variable -Name StateFlagNoAction -Value 0 -Option Constant

$ScriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$ScriptPath

#Phase Constants
Set-Variable -Name PhaseInit -Value -1 -Option Constant
Set-Variable -Name PhaseStarted -Value 0 -Option Constant
Set-Variable -Name PhaseDEInstalled -Value 1 -Option Constant
Set-Variable -Name PhaseSageSetComplete -Value 2 -Option Constant
Set-Variable -Name PhaseSageRunStarted -Value 3 -Option Constant
Set-Variable -Name PhaseSageRunComplete -Value 4 -Option Constant
Set-Variable -Name PhaseDERemoved -Value 5 -Option Constant
Set-Variable -Name PhaseTaskRemoved -Value 6 -Option Constant

#import-module
Import-Module ServerManager

#read state value, use switch statement

Function DateStamp {
    return "$(Get-Date -UFormat %Y%m%d-%H%M%S):"
}

Function LogEntry([string]$LogData) {
    Add-Content $LogPath "$(DateStamp) $LogData"
}

Function CreateScheduledTask {
    Param([string]$TaskName)
    try {
        $Scheduler = New-Object -ComObject "Schedule.Service"
        $Scheduler.Connect("Localhost")
        $root = $Scheduler.GetFolder("\")
        $newTask = $Scheduler.NewTask(0)
        $newTask.RegistrationInfo.Author = $TaskName
        $newTask.RegistrationInfo.Description = ""
        $newtask.Settings.StartWhenAvailable = $true
        $trigger = $newTask.Triggers.Create(1) #Trigger at boot
        $trigger.StartBoundary = [datetime]::Now.ToString("yyyy-MM-dd'T'HH:mm:ss")
        $trigger.Enabled = $true
        #$trigger.Delay = "PT1S"
        $trigger.Id = "LogonTriggerId"
        $newTask.Principal.UserId = "NT Authority\System"
        $newTask.Principal.RunLevel = 1
        $newTask.Principal.LogonType = 5

        $action = $newtask.Actions.Create(0)
        $action.Path = "C:\Windows\system32\cleanmgr.exe"
        
        $action.Arguments = "/sagerun:$sageset"

        $root.RegisterTaskDefinition("CleanMgr Cleanup Task", $newTask, 6, "NT AUTHORITY\SYSTEM", $null , 4)
    }
    catch {
        LogEntry "Failed to register scheduled task." 
        LogEntry $Error[0].Exception
        throw "Failed to register scheduled task..."
    }
}

Function DeleteScheduledTask {
    Param([string]$TaskName)
    c:\windows\system32\schtasks.exe /delete /TN "CleanMgr Cleanup Task" /f
}

Function GetCurrentState {
   return (Get-ItemProperty -Path $ScriptRegKey -Name $ScriptRegValueName -ErrorAction SilentlyContinue).Phase
}

if(Test-Path $ScriptRegKey) {
    $CurrentState = GetCurrentState
} else {
    $CurrentState = $PhaseInit
}

Function EndScript {
    try {
        LogEntry "PhaseTaskRemoved: Script Complete."

        $CurrentSpace = (Get-WmiObject win32_logicaldisk | where { $_.DeviceID -eq $env:SystemDrive }).FreeSpace
        LogEntry "PhaseTaskRemoved: Current Disk Space: $([Math]::Round(($CurrentSpace / 1GB),2)) GB"
        
        $Savings = [Math]::Round(((($CurrentSpace / $SpaceAtStart) - 1) * 100),2)

        $message = "****** CleanMgr complete.`n****** Starting Free Space: $SpaceAtStart`n****** Current Free Space: $CurrentSpace`n****** Savings: $Savings%`n****** Exiting."
        LogEntry $message
   }

   catch {
    LogEntry "PhaseTaskRemoved: Error during PhaseTaskRemoved..."
    LogEntry $Error[0].Exception
   }
   Stop-Transcript
   Exit
}

LogEntry "CurrentState: $CurrentState"
LogEntry "NoReboot Flag: $NoReboot"

do {
    LogEntry "**** Current State: $CurrentState"

    #Evalute current state against all possibilities.
    Switch($CurrentState) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    
    $PhaseInit {        
        LogEntry "Switch: Null"

        try {   
            #Calculate and log freespace        
            $FreeSpace = (Get-WmiObject win32_logicaldisk | where { $_.DeviceID -eq $env:SystemDrive }).FreeSpace
            if((Test-Path $ScriptRegKey) -eq $false)
            {
                New-Item -Path $ScriptRegKey
            }
            Set-ItemProperty -Path $ScriptRegKey -Name $ScriptSpaceBeforeValue -Value $FreeSpace
            LogEntry "PhaseInit: Current Free Space: $([Math]::Round(($FreeSpace / 1GB),2))GB"

            #If DE exists with no change needed or success, update reg keys. Reboot if required. Create task.
            LogEntry "PhaseInit: Updating $ScriptRegKey\$ScriptRegValueName to $PhaseStarted"
            #New-Item $ScriptRegKey -Force
            New-ItemProperty -Path $ScriptRegKey -Name $ScriptRegValueName -Value $PhaseStarted -Force
                
            LogEntry "PhaseInit: Continuing on to Disk Cleanup"

            $CurrentState = GetCurrentState
        }
        
        catch {
            LogEntry $error[0]
            Stop-Transcript
            exit
        }

        break
    }           

    $PhaseStarted {
        LogEntry "PhaseStarted: Creating Scheduled Task. Moving to PhaseDEInstalled."
        $scheduledTasks = schtasks /query /v /fo csv | ConvertFrom-Csv
        if (!($scheduledTasks -match "CleanMgr Cleanup Task")) { CreateScheduledTask -TaskName $SchTaskName }
        Set-ItemProperty -Path $ScriptRegKey -Name $ScriptRegValueName -Value $PhaseDEInstalled

        $CurrentState = GetCurrentState
        break
    }

    $PhaseDEInstalled {
        try {
            LogEntry "PhaseDEInstalled: Starting PhaseDEInstalled..."
            LogEntry "PhaseDEInstalled: Setting SagetSet..."
            #use static SageSet Value. Insert in to registry.
            Set-Variable -Name StateFlags -Value "StateFlags$SageSet" -Option Constant 
            LogEntry "PhaseDEInstalled: SageSet complete."
            LogEntry "PhaseDEInstalled: Setting VolumeCaches reg keys..."
            #Set all VolumeCache keys to StateFlags = 0 to prevent cleanup. After, set the proper keys to 2 to allow cleanup. 
            $SubKeys = Get-Childitem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VolumeCaches
            $SubKeys
            Foreach ($Key in $SubKeys) {
                #Set-ItemProperty -Path $Key.PSPath -Name $StateFlags -Value $StateFlagNoAction -PassThru
                Set-ItemProperty -Path $Key.PSPath -Name $StateFlags -Value $StateFlagClean
            }

            LogEntry "PhaseDEInstalled: VolumeCaches keys set."
            LogEntry "PhaseDEInstalled: Setting UPdate and Service Pack Keys..."
            #Set all script reg values for persistence through reboots.
            Set-ItemProperty -Path $ScriptRegKey -Name $ScriptSageValueName -Value $SageSet -PassThru
            #Set-ItemProperty -Path $UpdateCleanUpPath -Name $StateFlags -Value $StateFlagClean -PassThru
            #Set-ItemProperty -Path $ServicePackCleanUpPath -Name $StateFlags -Value $StateFlagClean -PassThru
            LogEntry "PhaseDEInstalled: Done." 

            #Update state key
            Set-ItemProperty -Path $ScriptRegKey -Name $ScriptRegValueName -Value $PhaseSageSetComplete -PassThru
            $CurrentState = GetCurrentState
            LogEntry "PhaseDEInstalled: Complete."
        }
        
        catch {
            LogEntry "PhaseDEInstalled: Failed to update reg keys."
            LogEntry $Error[0].Exception
        }
        break
    }

    $PhaseSageSetComplete {
        LogEntry "PhaseSageSetComplete: Starting cleanmgr.exe"
        try {
            $StartTime = Get-Date
            CD $env:systemdrive\temp
            #& cmd.exe /c "c:\Windows\System32\cleanmgr.exe /sagerun:$SageSet"
            schtasks /run /TN "CleanMgr Cleanup Task"
            LogEntry "PhaseSageSetComplete: CleanMgr.exe running... "
            Start-Sleep -Seconds 5
            Wait-Process cleanmgr
            $EndTime = Get-Date
            LogEntry "PhaseSageSetComplete: CleanMgr.exe complete..."
            LogEntry "PhaseSageSetComplete: Seconds Elapsed: $((New-TimeSpan $StartTime $EndTime).TotalSeconds)"
            LogEntry "PhaseSageSetComplete: Updating State..."
            Set-ItemProperty -Path $ScriptRegKey -Name $ScriptRegValueName -Value $PhaseSageRunComplete
            $CurrentState = GetCurrentState
            LogEntry "PhaseSageSetComplete: Complete."
        }

        catch {
            LogEntry "PhaseSageSetComplete: ERROR."            
            LogEntry $Error[0].Exception
            exit
        }    
        break    
    }

    $PhaseSageRunComplete {
        try {   
            LogEntry "PhaseSageRunComplete: Starting PhaseSageRunComplete."
            Set-ItemProperty -Path $ScriptRegKey -Name $ScriptRegValueName -Value $PhaseDERemoved            
            $CurrentState = GetCurrentState

        }

        catch {
            LogEntry "PhaseSageRunComplete: Caught Exception."
            LogEntry "$($Error[0].Exception)"
        } 
        break       
    }

    $PhaseDERemoved {
        try {
            #Retrieving initial space
            $SpaceAtStart = (Get-ItemProperty -Path $ScriptRegKey -Name $ScriptSpaceBeforeValue)."$ScriptSpaceBeforeValue"

            #remove reg key                        
            LogEntry "PhaseDERemoved: Removing Script Reg Key."
            Remove-Item $ScriptRegKey            
            $CurrentState = $PhaseTaskRemoved
            EndScript
        }

        catch {
            LogEntry "PhaseDERemoved: ERROR."            
            LogEntry $Error[0].Exception
        }
        break
    }

   
    }

#Prevents infinite loops consuming resources.
Sleep 1

} until ($CurrentState -eq $PhaseTaskRemoved)

