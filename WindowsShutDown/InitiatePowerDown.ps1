#Generated Form Function
function GenerateForm {
########################################################################
# Code Generated By: SAPIEN Technologies PrimalForms (Community Edition) v1.0.10.0
# Generated On: 10/2/2015 11:00 AM
# Generated By: juchurch-a
########################################################################

#region Import the Assemblies
[reflection.assembly]::loadwithpartialname("System.Windows.Forms") | Out-Null
[reflection.assembly]::loadwithpartialname("System.Drawing") | Out-Null
#endregion

#region Generated Form Objects
$form1 = New-Object System.Windows.Forms.Form
$prodbtn = New-Object System.Windows.Forms.Button
$qabtn = New-Object System.Windows.Forms.Button
$devbtn = New-Object System.Windows.Forms.Button
$InitialFormWindowState = New-Object System.Windows.Forms.FormWindowState
#endregion Generated Form Objects

#----------------------------------------------
#Generated Event Script Blocks
#----------------------------------------------
#Provide Custom Code for events specified in PrimalForms.
$handler_devbtn_Click= 
{
$script:mode = "DEV"
Invoke-Expression ("C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe -File `"$ScriptPath" + "\" + "$scriptname`" $mode")
$form1.Close()
}

$handler_qabtn_Click= 
{
$script:mode = "QA"
Invoke-Expression ("C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe -File `"$ScriptPath" + "\" + "$scriptname`" $mode")
$form1.Close()
}

$handler_prodbtn_Click= 
{
$script:mode = "PROD"
Invoke-Expression ("C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe -File `"$ScriptPath" + "\" + "$scriptname`" $mode")
$form1.Close()
}

$OnLoadForm_StateCorrection=
{#Correct the initial state of the form to prevent the .Net maximized form issue
	$form1.WindowState = $InitialFormWindowState
}

#----------------------------------------------
#region Generated Form Code
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 129
$System_Drawing_Size.Width = 121
$form1.ClientSize = $System_Drawing_Size
$form1.DataBindings.DefaultDataSourceUpdateMode = 0
$form1.Name = "form1"
$form1.Text = "Choose Environment"


$prodbtn.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 23
$System_Drawing_Point.Y = 89
$prodbtn.Location = $System_Drawing_Point
$prodbtn.Name = "prodbtn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$prodbtn.Size = $System_Drawing_Size
$prodbtn.TabIndex = 2
$prodbtn.Text = "PROD"
$prodbtn.UseVisualStyleBackColor = $True
$prodbtn.add_Click($handler_prodbtn_Click)

$form1.Controls.Add($prodbtn)


$qabtn.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 23
$System_Drawing_Point.Y = 50
$qabtn.Location = $System_Drawing_Point
$qabtn.Name = "qabtn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$qabtn.Size = $System_Drawing_Size
$qabtn.TabIndex = 1
$qabtn.Text = "QA"
$qabtn.UseVisualStyleBackColor = $True
$qabtn.add_Click($handler_qabtn_Click)

$form1.Controls.Add($qabtn)


$devbtn.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 23
$System_Drawing_Point.Y = 14
$devbtn.Location = $System_Drawing_Point
$devbtn.Name = "devbtn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$devbtn.Size = $System_Drawing_Size
$devbtn.TabIndex = 0
$devbtn.Text = "DEV"
$devbtn.UseVisualStyleBackColor = $True
$devbtn.add_Click($handler_devbtn_Click)

$form1.Controls.Add($devbtn)

#endregion Generated Form Code

#Save the initial state of the form
$InitialFormWindowState = $form1.WindowState
#Init the OnLoad event to correct the initial state of the form
$form1.add_Load($OnLoadForm_StateCorrection)
#Show the Form
$form1.ShowDialog()| Out-Null

} #End Function

#Call the Function
$script:ScriptPath = Split-Path -Parent $MyInvocation.MyCommand.Path
$script:scriptname = "PowerDown.ps1"

GenerateForm
Write-Host $mode