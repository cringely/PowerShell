#=============================================
# Automated Power off of VSQ Servers 
# Created by Justin Church, CSRA Inc
# Utility Function examples provided by Dwight Sowers, Microsoft Corp
# Build 2015-09-10 Revision 1
# Build 2015-09-30 Revision 2
# 
# Justin Church - CSC/FDIC Wintel Operations
#=============================================
<#
.SYNOPSIS 
    Ordered shutdown of VASQ datacenter
.EXAMPLE
    PowerDown.ps1 -env PROD
    This command finds and powers down any reachable windows servers.
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
.INPUTS
   First Parameter is the environment to shutdown {DEV | QA | PROD}
.OUTPUTS
   Logging and collection output will be placed in the Script's directory
#>
#=============================================
# Function:InitScript
# Arguments: None
#=============================================
# Purpose: Run script start up processes and 
# variables. Starts log files for this run
#=============================================
Param(
  [Parameter(Mandatory=$true,Position=1,ParameterSetName="Environment",HelpMessage="Environment to Shutdown: {DEV | QA | PROD}")]
  [alias("env","environment")]
  [string]$script:mode=$(throw "USAGE: PowerDown.ps1 -env {DEV | QA | PROD}")
)

Function AdminCheck
{
    $windowsSecurityPrincipal = New-Object System.Security.Principal.WindowsPrincipal([System.Security.Principal.WindowsIdentity]::GetCurrent())
    If (!($windowsSecurityPrincipal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)))
    {
        Write-Warning "Current user does not have Administrator rights"
        Write-Host "Restarting Script as Admin"
        $adminProcess = New-Object System.Diagnostics.ProcessStartInfo
        $adminProcess.Filename = ([System.Diagnostics.Process]::GetCurrentProcess()).Path
        $adminProcess.Arguments = " -File `"$ScriptPath" + "\" + "$scriptname`" $mode"
        $adminProcess.Verb = 'runas'
        Try
        {
            [System.Diagnostics.Process]::Start($adminProcess) | Out-Null
            Exit 0
        }
        Catch
        {
            Write-Error 'Could not start process'
            Exit 1
        }
    }
    Write-Host -ForegroundColor Green "`nScript Running as Admin`n"
}

Function InitScript
{
	#Import Necessary Modules and Declare necessary constants
	#Set-ExecutionPolicy Remotesigned
    $Script:VerbosePreference = "SilentlyContinue"
    $Script:DebugPreference = "SilentlyContinue"
	Set-StrictMode -version Latest
    Import-Module ActiveDirectory
    Add-PSSnapin VMware.VimAutomation.Core
    #Set-PowerCLIConfiguration -Confirm:$false -InvalidCertificateAction Ignore -Scope Session -ErrorAction Continue -WarningAction Continue| Out-Null
    $Script:VerbosePreference = "Continue"
    $Script:DebugPreference = "Continue"
    if ( $script:mode -like "dev")
    {  $script:mode = "1DEV"  }
    elseif ( $script:mode -like "qa")
    {  $script:mode = "2QA"   }
    elseif ( $script:mode -like "prod")
    {  $script:mode = "3PROD" }	
    else
    {
        $script:mode = "error"
        Write-Host -ForegroundColor Red "USAGE: PowerDown.ps1 -env {DEV | QA | PROD}"
        EXIT
    }

	# Open Log Files : Error, Debug and Result
	try
	{
		$script:ErrorLogFilename   =  $ScriptPath + "\Log_" + $scriptname + "_Error.txt"
		$script:DebugLogFilename   =  $ScriptPath + "\Log_" + $scriptname + "_Debug.txt"
		$script:ResultsLogFilename =  $ScriptPath + "\Log_" + $scriptname + "_Results.txt"
		$script:CurrentDT = Get-Date
		
		$ErrorLogStart = $scriptname + " script Run: Start " + $CurrentDT
		$scriptname + " script Run: Start " + $CurrentDT
		$DebugLogStart = "SCRIPT_PHASE,EVENT_TYPE,STATUS_MESSAGE,EVENT_TIMESTAMP"
		$ResultLogStart = "SCRIPT_PHASE,SERVER,RESULT,EVENT_TIMESTAMP"

		$ErrorLogStart | out-file -filepath $ErrorLogFilename -encoding "ASCII"
		if ($DebugLogToggle -eq "ON") 
		{
			$DebugLogStart | out-file -filepath $DebugLogFilename -encoding "ASCII"
			Write-Host "DEBUG Log is ON"
			Write-Host $DebugLogFilename
		}
		else
		{
			Write-Host "DEBUG Log is OFF"
		}
		$ResultLogStart | out-file -filepath $ResultsLogFilename -encoding "ASCII"
	}
	catch
	{
		[system.exception]
		write-host $error
		write-host $Error[0].InvocationInfo.PositionMessage
		"ERROR from InitScript. Exiting..." 
		exit
	}
    
}
#=============================================
# Function:WriteLog
# Arguments: LogType, MessageData, $ScriptPath
#=============================================
# Purpose: Appends the passed MessageData to the log type 
# specified
# EXAMPLE WriteLog "ERROR" "Log Message"
#=============================================
Function WriteLog ( $LogType, $MsgData)
{
	$MyCurrentDT = Get-Date
	# Determine Log Type: Error, Debug, Result
	if ($LogType -eq "RESULT") 
	{
		$LogFile = $ScriptPath + "\Log_" + $scriptname + "_Results.txt"
		$MsgData = $MsgData + "," + $MyCurrentDT
		$MsgData | out-file -filepath $LogFile -encoding "ASCII" -Append
	}
	elseif ($LogType -eq "ERROR") 
	{
		$LogFile = $ScriptPath + "\Log_" + $scriptname + "_Error.txt"
		$MsgData = $MsgData + "," + $MyCurrentDT
		$MsgData | out-file -filepath $LogFile -encoding "ASCII" -Append
	}
	elseif ($LogType -eq "DEBUG") 
	{
		# Check Debug Toggle
		if ($DebugLogToggle -eq "ON") 
		{
			$LogFile = $ScriptPath + "\Log_" + $scriptname + "_Debug.txt"
			$MsgData = $MsgData + "," + $MyCurrentDT
			$MsgData | out-file -filepath $LogFile -encoding "ASCII" -Append
		}
	}	
}
#=============================================
# Function:EndScript
# Arguments: None
#=============================================
# Purpose: Runs any script ending logic
#=============================================
Function EndScript
{
    $TotalDown = $Collection.count.ToString()
	$StartDT = $CurrentDT
    $CurrentDT = Get-Date
    $runtime = New-TimeSpan -Start $StartDT -End $CurrentDT
	$ErrorLogEnd = $scriptname + " script Run: End " + $CurrentDT
	"******************************************"
	"Review log files for script run results."
	"******************************************"
	$scriptname + " script Run: End " + $CurrentDT
    $scriptname + " script Execution Time: " + $runtime.Hours + "Hrs " + $runtime.Minutes + "Min " + $runtime.Seconds + "Sec"
    Write-Warning ($TotalDown + " Servers instructed to Shutdown")
	WriteLog "ERROR" $ErrorLogEnd
    Write-Host "`nPress Any Key to Exit Script...`n"
    [void][System.Console]::ReadKey($true)
    $Script:VerbosePreference = "SilentlyContinue"
    $Script:DebugPreference = "SilentlyContinue"
    $script:mode = $null
}

# Write the Collection to a file
Function SaveCollection
{
	writelog "DEBUG" "SaveCollection,INFO,Saving"
	$Collection |export-csv -notype -path ($ScriptPath + "\$script:mode.csv")
    $Collection | Out-GridView
}

# Utility function to manage Object Collection 
function Query-AD
{
    $Environments = @("*d.*.fdic.gov","*q.*.fdic.gov","*p.*.fdic.gov")
    foreach ($Environment in $Environments)
    {
        writelog "DEBUG" "Query-AD,INFO, Current Env = $Environment"
        
        if ( $Environment -eq "*d.*.fdic.gov")
        {  $CurrentEnv = "1DEV"   }
        elseif ( $Environment -eq "*q.*.fdic.gov")
        {  $CurrentEnv = "2QA"   }
        elseif ( $Environment -eq "*p.*.fdic.gov")
        {  $CurrentEnv = "3PROD"   }

        if($CurrentEnv -eq $script:mode)
        {
            $WebSvrFilter = "enabled -eq `"$true`" -and
                            (name -like '*iis*' -or name -like '*web*') -and 
                            (name -like 'was*' -or name -like 'vasw*') -and 
                            name -notlike '*rp' -and
                            operatingsystem -like '*windows server*' -and 
                            dnshostname -like `"$Environment`" -and 
                            PasswordLastSet -gt `"$time`" ".replace("rp","r"+$script:mode.Substring(1,1))

            $AppSvrFilter = "enabled -eq `"$true`" -and
                            (name -notlike '*sql*' -and 
						    name -notlike '*inf*' -and
                            name -notlike '*wapp173p*' -and
                            name -notlike '*wapp174p*' -and
                            name -notlike '*vso*' -and 
						    name -notlike '*iis*' -and 
						    name -notlike '*web*' -and 
						    name -notlike '*bbs*' -and 
						    name -notlike '*exc*' -and 
						    name -notlike '*evm*' -and 
						    name -notlike '*bes*' -and
                            name -notlike '*vmc*' -and
                            name -notlike '*rp' -and
						    name -notlike `"$env:COMPUTERNAME`") -and
                            (name -notlike 'wasres2*' -and name -notlike 'wasres3*' -and name -notlike 'wasres4*') -and
						    (name -like 'was*' -or name -like 'vasw*') -and 
                            operatingsystem -like '*windows server*' -and 
						    dnshostname -like `"$Environment`" -and 
                            PasswordLastSet -gt `"$time`" ".replace("rp","r"+$script:mode.Substring(1,1))


            $SQLSvrFilter = "name -notlike '*ap*' -and 
                            name -notlike '*bp*' -and 
                            name -notlike '*cp*' -and 
                            name -notlike '*dp*' -and 
                            name -notlike '*ep*' -and 
                            name -notlike '*fp*'".replace("p",$script:mode.Substring(1,1))

            $SQLSvrFilter = "enabled -eq `"$true`" -and
                            operatingsystem -like '*windows server*' -and 
                            (name -like '*sql*' -and 
                            (name -like 'was*' -or name -like 'vasw*') -and 
                            ($SQLSvrFilter)) -and 
                            dnshostname -like `"$Environment`" -and 
                            PasswordLastSet -gt `"$time`""

            $CLSSvrFilter = "name -like '*ap*' -or 
                            name -like '*bp*' -or 
                            name -like '*cp*' -or 
                            name -like '*dp*' -or 
                            name -like '*ep*' -or 
                            name -like '*fp*' -and
                            name -notlike '*vmc*'".replace("p",$script:mode.Substring(1,1))

            $CLSSvrFilter = "enabled -eq `"$true`" -and
                            operatingsystem -like '*windows server*' -and 
                            (name -like '*sql*' -and 
                            (name -like 'was*' -or name -like 'vasw*') -and 
                            ($CLSSvrFilter)) -and 
                            dnshostname -like `"$Environment`" -and 
                            PasswordLastSet -gt `"$time`""

            $VMwareFilter = "enabled -eq `"$true`" -and
                            (name -like '*vmc*' -or name -like '*vso*' -or name -like '*wapp173p*' -or name -like '*wapp174p*') -and 
                            (name -like 'was*' -or name -like 'vasw*') -and 
                            operatingsystem -like '*windows server*' -and 
                            dnshostname -like `"$Environment`" -and 
                            PasswordLastSet -gt `"$time`" ".replace("rp","r"+$script:mode.Substring(1,1))


            $WEBServers  = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter $WebSvrFilter | where {$_.DistinguishedName -notlike "*Tombstone*"}
            $APPServers  = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter $AppSvrFilter | where {$_.DistinguishedName -notlike "*Tombstone*"}
            $SQLServers  = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter $SQLSvrFilter | where {$_.DistinguishedName -notlike "*Tombstone*"}
            $DCs         = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter {operatingsystem -like "*windows server*" -and name -like "*inf*" -and (name -like "was*" -or name -like "vasw*") -and dnshostname -like $Environment -and PasswordLastSet -gt $time} | where {$_.DistinguishedName -notlike "*Tombstone*"}
            $VMware      = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter $VMwareFilter | where {$_.DistinguishedName -notlike "*Tombstone*"}
            $SQLClusters = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter $CLSSvrFilter | where {$_.DistinguishedName -notlike "*Tombstone*"}

            #Comment above and Uncomment below for DESS LAB TEST. Error will display for each server type due to user object in AD group

            #$WEBServers  = Get-ADGroupMember POWER_Test|Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem |where {$_.operatingsystem -like "*windows server*" -and ($_.Name -like "*iis*" -or $_.name -like "*web*") -and ($_.name -like "was*" -or $_.name -like "vasw*") -and $_.dnshostname -like $Environment -and $_.PasswordLastSet -gt $time}
            #$APPServers  = Get-ADGroupMember POWER_Test|Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem |where {$_.operatingsystem -like "*windows server*" -and ($_.name -notlike "*sql*" -and $_.name -notlike "*inf*" -and $_.name -notlike "*iis*" -and $_.name -notlike "*web*" -and $_.name -notlike "*bbs*" -and $_.name -notlike "*exc*" -and $_.name -notlike "*evm*" -and $_.name -notlike "*bes*") -and ($_.name -like "was*" -or $_.name -like "vasw*") -and $_.dnshostname -like $Environment -and $_.PasswordLastSet -gt $time}
            #$SQLServers  = Get-ADGroupMember POWER_Test|Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem |where {$_.operatingsystem -like "*windows server*" -and ($_.Name -like "*sql*" -and ($_.name -like "was*" -or $_.name -like "vasw*") -and ($_.name -notlike "*ap*" -and $_.name -notlike "*bp*" -and $_.name -notlike "*cp*" -and $_.name -notlike "*dp*" -and $_.name -notlike "*ep*" -and $_.name -notlike "*fp*")) -and $_.dnshostname -like $Environment -and $_.PasswordLastSet -gt $time}
            #$SQLClusters = Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem -Filter {name -like "VASWSQL100BP" -or name -like "VASWSQL200BP"}
            #$DCs         = Get-ADGroupMember POWER_Test|Get-ADComputer -Properties PasswordLastSet,DNSHostName,IPv4Address,OperatingSystem |where {$_.operatingsystem -like "*windows server*" -and ($_.Name -like "*inf*") -and ($_.name -like "was*" -or $_.name -like "vasw*") -and $_.dnshostname -like $Environment -and $_.PasswordLastSet -gt $time}

            $ResultsArray = @($WEBServers,$APPServers,$SQLServers,$DCs,$SQLClusters,$VMware)
            $Script:ServerType = @("1WEBServers","2APPServers","3SQLServers","4DCs","5SQLClusters","6VMware")
            $ServerTypeCounter = 0
            foreach ($Result in $ResultsArray)
            {
                $global:servercount = 0
                $CurrentServerType = $ServerType[$ServerTypeCounter]
                writelog "DEBUG" "Query-AD,INFO, CurrentServerType = $CurrentServerType"
                if($CurrentEnv -eq $script:mode)
                { 
                    Write-Host `n
                    Write-Host -ForegroundColor Cyan ("Found "+@($result.count)+" "+$CurrentServerType.substring(1)+" in "+$currentenv.substring(1)+", testing if reachable.`n") 
                }
                If($Result)
                {
                    foreach ($Server in $Result)
                    {
                        Find-IsAlive $Server
                        if($Script:RPCCheck -eq $true -or $Script:WINRMCheck -eq $true -or $aliveresult -eq $true)
                        {
                            writelog "DEBUG" ("Query-AD,INFO,ServerName |"+$Server.name+"| CurrentType |$CurrentServerType| AliveResult |$aliveresult|")

                            $NewObj = New-Object System.Object
	                        $NewObj | Add-Member -type NoteProperty -name "ServerName" -value ($Server.name)
                            $NewObj | Add-Member -type NoteProperty -name "DNSName" -value ($Server.dnshostname)
	                        $NewObj | Add-Member -type NoteProperty -name "Operating System" -value ($Server.operatingsystem)
                            $NewObj | Add-Member -type NoteProperty -name "IPV4" -value ($Server.IPv4Address)
                            $NewObj | Add-Member -type NoteProperty -name "Environment" -value $CurrentEnv
                            $NewObj | Add-Member -type NoteProperty -name "ServerType" -value $CurrentServerType
                            $NewObj | Add-Member -type NoteProperty -name "IsAlive" -value $AliveResult
		
	                        $Global:Collection  += $NewObj 	
                        }
                        elseif ($aliveresult -eq $false)
                        { Writelog "RESULT" ("Query-AD,"+ $server.name +",Skipped,"+ $server.name +" not reachable") }
                        else 
                        { writelog "ERROR" "Query-AD,ERROR,$server,Unreachable" }
                    }
                }
                else
                {
                    writelog "DEBUG" "Query-AD,INFO,No results found for $Result"
                }
                $ServerTypeCounter += 1
            }
        }
    }
}

function Find-IsAlive($srv)
{
    $srvd = $srv.dnshostname
    $srvip = $srv.ipv4address
    $srv = $srv.name
    $Script:AliveResult = $null
    $Script:RPCCheck = $null
    $Script:WINRMCheck = $null

    WriteLog "DEBUG" "Find-IsAlive,INFO,PING test on |$srv|"
    if (Test-Connection $srvd -Count 2 -ErrorAction SilentlyContinue -Quiet)
    {
        $global:servercount += 1
        WriteLog "DEBUG" "Find-IsAlive,INFO,PING test on |$srv| Successful."
        $Script:AliveResult = $true
        if (Get-WmiObject win32_computersystem -ComputerName $srvd -ErrorAction SilentlyContinue -Authentication 6)
        {
            WriteLog "DEBUG" "Find-IsAlive,INFO,RPC connection on $srv successful."
            Write-Host -ForegroundColor Cyan "Reached $srvd via RPC, adding to Shutdown queue. - $global:servercount"
            $Script:RPCCheck = $true
        }
        elseif (Test-WSMan -ComputerName $srvip -ErrorAction SilentlyContinue) 
        {
            $Script:WINRMCheck = $true
            WriteLog "DEBUG" "Find-IsAlive,INFO,NORPC connection to $srv"
            WriteLog "DEBUG" "Find-IsAlive,INFO,WinRM connection on $srv successful."
            Write-Host -ForegroundColor Cyan "Reached $srvd via WinRM, adding to Shutdown queue. - $global:servercount"
        }
        else
        {
            $global:errorcount += 1
            WriteLog "DEBUG" "Find-IsAlive,ERROR,Unable to connect to |$srvd|"
            WriteLog "ERROR" ("Find-IsAlive,ERROR,"+$error[0].Exception+" - "+$Error[0].InvocationInfo.line)
            Write-Warning "Pinged $srvd but it is unreachable via RPC/WMI and WinRM. - $global:servercount"
            $unreachable += $srvd
            $Script:AliveResult = $false
            $Script:RPCCheck = $false
            $Script:WINRMCheck = $false
        }
    }
    else
    {
        $global:errorcount += 1
        $global:servercount += 1
        WriteLog "DEBUG" "Find-IsAlive,ERROR,Unable to connect to |$srvd|"
        WriteLog "ERROR" ("Find-IsAlive,ERROR,"+$error[0].Exception+" - "+$Error[0].InvocationInfo.line)
        Write-Warning "Found $srvd in Directory, no ICMP response. SKIPPING - $global:servercount"
        $unreachable += $srvd
        $Script:AliveResult = $false
        $Script:RPCCheck = $false
        $Script:WINRMCheck = $false
    }
}

function Shutdown-Servers
{
    $global:servercount = 0
    $Script:TypeCount = 0
    Foreach ($type in $ServerType)
    {
        Writelog "DEBUG" "Shutdown-Servers,INFO,Current type is |$type| mode is |$script:mode|"
        $Script:TypeCount += 1
        if ($Type -like "*SQLC*" -and $Script:ESXON -ne $false -and $script:mode -like "*prod") 
        {
            Write-Warning "Shutdown ESX Hosts"
            Shutdown-ESX
            $Script:ESXON = $false
        }
        # This creates a new filtered collection for the current environment (passed by mode) and for the current server type which is passed from the foreach inside this function.
        # This syntax is probably wrong but it should be close to this.
        $CurrentCollection = $Global:Collection | where {$_.servertype -eq $type}
        Write-Host -ForegroundColor Cyan ("`nExecuting shutdown on "+$CurrentCollection.count+" reachable servers in "+$type.SubString(1)+" collection.`n")
        # Run Collection through ShutdownServers
        foreach ($server in $CurrentCollection)
        {
            $computer = $server.servername
            Writelog "DEBUG" "Shutdown-Servers,INFO,|$computer| aliveresult is |$AliveResult|"
            try
            {
                $global:servercount += 1
                WriteLog "DEBUG" "Shutdown-Servers,INFO,$computer is part of $type"
                Stop-Computer -ComputerName $computer -Force -Verbose
                WriteLog "RESULT" "Shutdown-Servers,$computer,SHUTDOWN sent"
                if($type -like "*SQLC*")
                { Start-Sleep -Seconds 30 }
                else
                { Start-Sleep -Milliseconds 500 }
            }
            catch
            {
                $global:servercount += 1
                Write-Warning ("SHUTDOWN TO " + $computer.ToUpper() + " REFUSED! - $servercount")
                WriteLog "RESULT" "Shutdown-Servers,ERROR,Shutdown command unsuccessful to |$server|"
                WriteLog "ERROR" ("Shutdown-Servers,ERROR,$computer - " + $error[0].Exception)
            } 
        }
    }
}

function Shutdown-ESX
{
    $vcs = @("vaswvmc150p","vaswapp173p","vaswvmc001p","vaswvmc004p")
    foreach ($vc in $vcs)
    {
        Connect-VIServer -Server $vc
        foreach ($vmhost in $esxforce)
        {
            Write-Host -ForegroundColor DarkCyan $vmhost.Name
            Get-VM * |where {$_.vmhost -like $vmhost.Name -and $_.Powerstate -ne "PoweredOff"}| Shutdown-VMGuest -Confirm:$false
        }
        Get-VMHost * |where {$_.PowerState -ne "PoweredOff" -and ($_.Name -like "vas*" -or $_.Name -like "was*")} |Stop-VMHost -RunAsync -Confirm:$false -RunAsync
        Write-Host -ForegroundColor Cyan "Pause for hosts to powerdown..."
        Start-Sleep -Seconds 20
    }
}

#*=============================================
#* SCRIPT BODY
#*=============================================
$script:ScriptPath = Split-Path -Parent $MyInvocation.MyCommand.Path
$script:scriptname = "PowerDown.ps1"
$script:DebugLogToggle = "ON"
$Script:RPCCheck = $null
$Script:WINRMCheck = $null
$Script:ESXON = $true
$DataFilePath = $ScriptPath + "\Output.csv"
$script:time = (Get-Date).AddDays(-240)
$global:errorcount = 0 
$GLOBAL:Collection = @()
$error.clear()
AdminCheck
InitScript
Write-Host -ForegroundColor Cyan ("Finding servers in "+$script:mode.Substring(1)+" environment`n")
Query-AD
Write-Host `n
Write-Host -ForegroundColor Cyan ("Found " + $GLOBAL:Collection.count + " reachable servers")
Write-Host -ForegroundColor Red "`nAttempting shutdown of reachable servers`n"
Shutdown-Servers
SaveCollection
EndScript 
#*=============================================
#* END OF SCRIPT:
#*=============================================