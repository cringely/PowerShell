<#
.SYNOPSIS
Automate USMT backup to copy only profile and minimal local data. This script will also attempt to auto generate the Loadstate script.

.DESCRIPTION
Original Author: Justin Church - CSRA/DESS
Author Date: 8.3.2016

.PARAMETER LogPath (optional)
Set custom logpath for script execution errors. Defaults to C:\temp\USMT.log

.EXAMPLE
& USMT.ps1 -LogPath C:\directory\to\log

.PARAMETER Target (optional)
Provide the hostname of the new workstation. Will prompt for name if no value proved at commandline

.EXAMPLE
& USMT.ps1 -Target hostname
#>

Param(
    [string]$Script:LogPath="c:\temp\USMT$((Get-Date).ToString('yy-MM-dd')).log",
    [Parameter(Mandatory=$False,ParameterSetName="Target",Position=1)]
    [alias("target","tar")]
    [string]$Script:Target="EDIT ME"
)

#Set variables and define logging
Begin {
    #Start Console log
    Start-Transcript -Force -IncludeInvocationHeader -Path C:\temp\USMTTranscript.log -ErrorAction SilentlyContinue
    $ErrorActionPreference = "Continue"
    $ScriptPath  = Split-Path -Parent $MyInvocation.MyCommand.Path
    #Determine SMS server
    $ParentPath = [string]::join("\",$scriptpath.Split("\")[2])
    $Destination = Import-Csv $scriptpath\usmtconfig.csv
    #Load gui generating library
    [System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
    if ($Script:Target -like "EDIT ME") {
        $Script:Target = [Microsoft.VisualBasic.Interaction]::InputBox("Enter new machine name", "Computer name", "")
    }
    #Search AD without needing RSAT installed (Import-Module ActiveDirectory)
    $Searcher = [adsisearcher] "(&(objectclass=computer)(name=$Script:Target))"
    $DomComputer = $Searcher.FindAll()
    if ($Script:Target -eq $null) {
        Write-Host "Required field missing: Name of New Computer. Please re-run script" -ForegroundColor Red
        Pause
        Stop-Transcript
        Exit
    } elseif ($Script:Target -match $DomComputer) {
        #Exit is new machine has no AD object
        Write-Host "Specified new computer is not domain joined. Please add it to the domain first!"  -ForegroundColor Red
        Pause
        Stop-Transcript
        Exit
    }
    Remove-Item "\\$Script:Target\C$\SOURCE\Win7_USMT\LoadState64.cmd" -Force -ErrorAction SilentlyContinue

    Function WriteLog([string]$LogData) {
    <#
    .Example
    WriteLog "Info you want to write to the log"
    #>
        Add-Content $LogPath "$(Get-Date -UFormat %Y%m%d-%H%M%S) $LogData"
    }

}

#Begin the heavy lifting
Process {
    #Determine UserData backup path
    if ($Destination -match $ParentPath) {
        $Script:BackupPath = "\\$(($Destination | Where {$_ -match $ParentPath} | Select -First 1).Destination)\Userdata$\$env:ComputerName"
        Write-Host "Backup location is $BackupPath" -ForegroundColor Green
        WriteLog "Backup location is $BackupPath"
    } else {
        Write-Host "$ParentPath not found, are you executing from the right SMS server?" -ForegroundColor Red
        WriteLog "$ParentPath not found"
        Pause
        Return
    }
    #Copy USMT binaries locally
    Write-Host "Copying USMT to local disk..." -ForegroundColor Yellow
    Copy-Item "$ScriptPath\USMT\" "C:\SOURCE\Win7_USMT\" -Force -Recurse
    Copy-Item "$ScriptPath\Laptop2016\*" "C:\SOURCE\Win7_USMT\" -Force -Recurse
    Set-Location C:\SOURCE\Win7_USMT

    Try {
        #Start profile backup
        Write-Host "Running USMT Backup Process now..." -ForegroundColor Yellow
        WriteLog "Starting USMT ScanState"
        Start-Process -Wait -FilePath 64\scanstate.exe -ArgumentList "$BackupPath /c /localonly /v:13 /uel:21 /progress:C:\SOURCE\Win7_USMT\Logs\ScanState_Progress.log /l:C:\SOURCE\Win7_USMT\Logs\USMT_Scan.log /o /efs:decryptcopy"
        Write-Host "Finished USMT ScanState" -ForegroundColor Green
        WriteLog "Finished USMT ScanState"
        Sleep 5
    } Catch {
        Write-Host "Unable to run USMT ScanState, is the EXE missing?`nReview logs at C:\SOURCE\Win7_USMT\Logs" -ForegroundColor Red
        WriteLog "Unable to run USMT ScanState"
        Pause
        Return
    }

    #Test is new machine is on the network
    if (Test-Connection -ComputerName $Script:Target -Count 1 -Quiet) {
        #Test if directory exists and if we have permissions to write
        if (!(Test-Path \\$Script:Target\c$\SOURCE\Win7_USMT -ErrorAction SilentlyContinue)) {
            Write-Host "Unable to connect or Access denied to new machine at \\$Script:Target\c$\SOURCE\Win7_USMT" -ForegroundColor Red
            Sleep 5
            Return
        } else {
            #Create USMT directory on new machine
            New-Item -ItemType Directory \\$Script:Target\c$\SOURCE\Win7_USMT
        }

        #Generate content of LoadState64.cmd
        Write-Host "$Target is reachable, attempting to generate LoadState file" -ForegroundColor Yellow
        $Output = "Copy /Y $ScriptPath\USMT\64\laptopcustom.xml C:\SOURCE\Win7_USMT\64\*.*`n"
        $Output += "C:\SOURCE\Win7_USMT\64\loadstate.exe $BackupPath /i:C:\SOURCE\Win7_USMT\64\laptopcustom.xml /ue:PROD\*-D /ue:PROD\*-A /ue:$env:computername\* /c /v:13 /progress:C:\SOURCE\Win7_USMT\Logs\Restore_Progress.log /l:C:\SOURCE\Win7_USMT\Logs\USMT_Restore.log"
        try {
            #Attempt to write content to disk on new machine
            $Output | Out-File "\\$Script:Target\C$\SOURCE\Win7_USMT\LoadState64.cmd" -Force -Encoding default
            Write-Host "File Created `nRun C:\SOURCE\Win7_USMT\LoadState64.cmd Located on $Script:Target" -ForegroundColor Green
            WriteLog "Created LoadState file on new workstation $Script:Target"
            Sleep 5
        } catch {  
            if (!(Test-Path "\\$Script:Target\C$\SOURCE\Win7_USMT\LoadState64.cmd")) {
                Write-Host "Failed to create LoadState Script on $Script:Target, do you have sufficient permissions?" -ForegroundColor Red
                WriteLog "Failed to create LoadState Script on $Script:Target"
                pause
                Return
            }
            Return
        }
    } else {
        Throw "$Script:Target is not pingable, unable to write LoadState64 file!`n"
        exit
    }    
}

End {
    #Close logs, exit script
    Write-Host "`nScript Execution complete, if any errors (Red Text) please correct and re-run `nScript will auto close in 10 seconds" -ForegroundColor Yellow
    WriteLog "USMT Process complete!"
    Stop-Transcript
    Sleep 10
    Exit
}