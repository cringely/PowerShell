Do {
    #Count how many VMs remain for migration.
    $count = (Get-Datastore "*D-Netapp*" | Get-VM | Where {$_.Name -like "VW1*"}).count
    #Determine how much Free Space remains, Best practice is to leave no less than 20%
    $DS = Get-Datastore "*vas003d pure*" | where {$_.name -notlike "*010"} | Get-Random
    $Free = ((Get-DataStore $DS).FreeSpaceGB / (Get-DataStore $DS).CapacityGB * 100)
    #Present Data to console
    Write-Host ([math]::Round($Free,2))"% Free space on $DS remaining.." -ForegroundColor Yellow
    Write-Host "$count VW1 machines remain.." -ForegroundColor Green
    Write-Host "Attempting 5 storage vMotions..`n" -ForegroundColor DarkGreen
    #Randomly select 5 VMs that aren't already on PURE
    Get-Datastore "*1D-Netapp*" | Get-VM | Where {$_.Name -like "VW1*"} | Get-Random -count 5 | Move-VM -Datastore (Get-Datastore $DS) -DiskStorageFormat Thin 3>>c:\temp\migratewarning.txt 2>>c:\temp\migrateerror.txt
    #Pause for an arbitrary amount of time
    Write-Host "`nPausing for 15 seconds" -ForegroundColor Magenta
    Start-sleep -s 15
    #Loop until no VMs remain on spinning disks or if target LUN is below 20% free space
} UNTIL ((Get-Cluster "VAS003D-Netapp Cluster" | Get-VM | Where {$_.Name -like "VW1*" -and ((get-view -id $_.DatastoreIdList[0]).name) -notlike "*VAS003D PURE*"}).count -eq "0" `
 -or ((Get-DataStore $DS).FreeSpaceGB / (Get-DataStore $DS).CapacityGB) -le ".20")