$dscName = "COGD-U450-DEVTEST-DATASTORE-CLUSTER"
$simCount = "4"
$clusName = "COGD-DevTest-Cluster"
$vcenter = "cogdpvc0004"
$dsName = 'COGD-U450-DEVTEST-DSTORE-*_U550'
$cred = Get-Credential

Do {
    #Build VM collection
    $vms = Get-DatastoreCluster $dscName | Get-VM
    $count = $vms.count
    $i = 0
    $block = {
        Param(
            $vm,
            $vcenter,
            [System.Management.Automation.CredentialAttribute()]$cred,
            [string]$dsName
            )
        $null = Connect-VIServer -Server $vcenter -Force -Credential $cred
        #Grab a random Datastore based off name filter
        $DS = Get-Datastore $dsName | Get-Random
        #svMotion to random datastore
        Get-VM $vm | Move-VM -Datastore (Get-Datastore $DS) -DiskStorageFormat Thin -WhatIf
    }
    #Remove all jobs
    Get-Job | Remove-Job
    $MaxThreads = $simCount
    #Start the jobs. Max jobs = $simcount, running simultaneously.
    foreach($vm in $vms){
        $i++
        Write-Progress -Activity "Storage vMotions" -Status "$i out of $count VMs" -PercentComplete (($i / $count) * 100)
        While ($(Get-Job -state running).count -ge $MaxThreads){
            Start-Sleep -seconds 1
        }
        Start-Job -Scriptblock $Block -ArgumentList $vm,$vcenter,$cred,$dsName -Name $vm
    }
    #Wait for all jobs to finish.
    While ($(Get-Job -State Running).count -gt 0){
        start-sleep 1
    }
    #Get information from each job.
    foreach($job in Get-Job){
        $info = Receive-Job -Id ($job.Id)
        $info
    }
    #Arbitrary Pause
    Start-Sleep -Seconds 15
    #Loop until no VMs remain on source disks or if target LUN is below 20% free space
} UNTIL ((Get-DataStoreCluster $dscName | Get-VM).count -eq "0" -or ((Get-DataStore $DS).FreeSpaceGB / (Get-DataStore $DS).CapacityGB) -le ".20")
