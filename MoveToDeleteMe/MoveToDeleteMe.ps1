$DSdrives = Get-Datacenter "vas dev" | Get-Datastore | where {$_.Name -notlike "*pure*" -and $_.Name -notlike "*wks*" -and $_.Name -notlike "*local*" -and $_.Name -Notlike "*transfer*" -and ($_ | Get-VM).count -lt 1}
$OrigPath = pwd

foreach ($DSdrive in $DSdrives) {
    $folders = $null
    Write-Host "`nConnecting to "$DSdrive.Name -ForegroundColor Gray
	New-PSDrive -location $DSdrive -Name ($DSdrive.Name) -PSprovider VimDatastore -Root "\" | Out-Null
	Set-Location $DSdrive`:
	$folders = ls | where {$_.Name -notlike "*deleteme*" -and $_.Name -ne ".vsphere-ha" -and $_.Name -ne ".dvsdata"}
    if ($folders -ne $null) {
        Write-Host "`nAttempting to move the following potentially orphaned folders to .deleteme`n" -ForegroundColor Red
        $folders | Select DatastoreFullPath
        Start-Sleep 1
        foreach ($folder in $folders) {
            if (!(Test-Path .deleteme\)) {mkdir .deleteme}
		    move-item -Path $folder.PSpath -Destination .deleteme\
	    }
    } Else {Write-Host "`nNo orphaned folders found, or already in .deleteme folder!" -ForegroundColor Green}
    Remove-PSDrive -Name ($DSdrive.Name) -Force
    Start-Sleep 1
}

Set-Location $OrigPath.Path