#List of trouble servers provided by Storage Team, comma separated.
$servers = @("vaswweb011i,vaswweb009i,vaswweb008i,vaswapp2012c")
#$servers.split(",")
$Script:fails = @()

foreach ($server in $servers.split(",")) 
{
    #Test connectivity to server
    if (Test-Connection -ComputerName $server -Count 1 -Quiet) 
        {
        #Test is RPC is usable, otherwise forgo attempt to restart
        if (Get-WmiObject -Class win32_ComputerSystem -ComputerName "$server" -Namespace root\cimv2 -ErrorAction SilentlyContinue)
            {
            Write-Host $server -ForegroundColor Green
            taskkill.exe /s $server /IM "FrameWorkService.exe" /F
            #sc.exe \\$server STOP "McAfeeFramework"
            Start-Sleep -s 3
            sc.exe \\$server START "McAfeeFramework"
            Start-sleep -s 1
            }
        else
            {
            Write-Host $server "RPC unreachable!" -ForegroundColor Red
            $fails += "$server NO RPC`n"
            }
        }
    else 
    {
    Write-host $server "Not reachable" -ForegroundColor Red
    $fails += "$server NO PING`n"
    }
}

#List servers we couldn't connect to
if ($fails.Count -lt "1" ) {Break}
else {Write-Host "The following server(s) require manual inspection:"`n $fails}