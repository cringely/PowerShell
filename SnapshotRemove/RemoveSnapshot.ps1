<#----------------------------------------------------------------------------
Original Author: 
Modified by: Justin Church, CSRA

Usage: .\RemoveSnap.Ps1 [-vCenter <string>] [-LogPath <String>]

Switch: vCenter - Specify the vcenter server to connect to.

        Exclude - Specify path to exclusion list

        LogPath - Specify this parameter with a log location to write out the script log.
                  Will default to snaplog$((Get-Date).ToString('yy-MM-dd')).txt in the c:\temp directory.

-----------------------------------------------------------------------------#>
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true,HelpMessage="vCenter server to connect to")]
    [string]$Script:vcenter=$(throw "-vCenter is required."),
    [Parameter(Mandatory=$true,HelpMessage="Path to exclusion file")]
    [string]$Script:exclude=$(throw "-exclude is required."),
    [string]$Script:LogPath="c:\temp\snaplog$((Get-Date).ToString('yy-MM-dd')).txt"
)

Begin {
    if (-not (Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue)) {
	    Add-PSSnapin VMware.VimAutomation.Core
    }

    Function DateStamp {
        return "$(Get-Date -UFormat %Y%m%d-%H%M%S):"
    }

    Function LogEntry([string]$LogData) {
        Add-Content $LogPath "$(DateStamp) $LogData"
    }

    Write-Host -ForegroundColor Yellow "Connecting to $vcenter and importing exclusion list"
    connect-viserver $vcenter

    $exclusions = Get-Content $exclude
    Write-Host -ForegroundColor Yellow "Set Exclusion list to $($exclusions.Split())"
    
    $Script:i = 0
}

Process {
    Write-Host -ForegroundColor Yellow "Finding and removing snapshots.."

    $snaps = Get-VM | Get-Snapshot | where {$exclusions -notcontains $_.vm -and $_.Created -lt $((Get-Date).AddDays(-30))}
    $snaps | Sort VM | foreach {LogEntry "Found Snapshot of $($_.VM) created on $($_.Created)"}
    Foreach ($snap in $snaps) {
        Write-Progress -Activity "Removing snapshots older than 30 days" -CurrentOperation "$($snap.VM) - $($snap.Created)" -Id 1 -PercentComplete (($i/$snaps.count)*100) -Status "Removing Snap $($i+1) of $($snaps.count)"
        LogEntry "Attempting to remove snapshot for $($snap.VM) from $($snap.Created)"
        try {
            $snap | Remove-Snapshot -WhatIf
            $i++
            Write-Host -ForegroundColor Green "Removed snapshot for $($snap.VM) from $($snap.Created)"
            LogEntry "Removed snapshot for $($snap.VM) from $($snap.Created)"
            Start-Sleep -s 1
        }
        catch {
            $i++
            Write-Host -ForegroundColor Red "Failed to remove snapshot for $($snap.VM) from $($snap.Created)"
            LogEntry "Failed to remove snapshot for $($snap.VM) from $($snap.Created)"
        }
    }
}

End {
    LogEntry "Removed $($snaps.Count) Snapshots that were older than 30 days and not contained within Exclusion file"
}