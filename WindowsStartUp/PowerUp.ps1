
#=============================================
# Function:InitScript
# Arguments: None
#=============================================
# Purpose: Run script start up processes and 
# variables. Starts log files for this run
#=============================================
Function InitScript
{
	# Import Necessary Modules and Declare necessary constants
	#Set-ExecutionPolicy Remotesigned
	Set-StrictMode -version Latest
	
	# Open Log Files : Error, Debug and Result
	try
	{

		$script:ErrorLogFilename   =  $ScriptPath + "\Log_" + $scriptname + "_Error.txt"
		$script:DebugLogFilename   =  $ScriptPath + "\Log_" + $scriptname + "_Debug.txt"
		$script:ResultsLogFilename =  $ScriptPath + "\Log_" + $scriptname + "_Results.csv"
		$script:CurrentDT = Get-Date
		
		$ErrorLogStart = $scriptname + " script Run: Start " + $CurrentDT
		$scriptname + " script Run: Start " + $CurrentDT
		$DebugLogStart = "EVENT_TYPE,SCRIPT_PHASE,Object,STATUS_MESSAGE,EVENT_TIMESTAMP"
		$ResultLogStart = "BU,FirstName,LastName,Email,Domain"

		$ErrorLogStart | out-file -filepath $ErrorLogFilename -encoding "ASCII"
		if ($DebugLogToggle -eq "ON") 
		{
			$DebugLogStart | out-file -filepath $DebugLogFilename -encoding "ASCII"
			#Write-Host "DEBUG Log is ON"
			#Write-Host $DebugLogFilename
		}
		else
		{
			#Write-Host "DEBUG Log is OFF"
		}
		$ResultLogStart | out-file -filepath $ResultsLogFilename -encoding "ASCII"
	}
	catch
	{
		[system.exception]
		write-host $error
		write-host $Error[0].InvocationInfo.PositionMessage
		"ERROR from InitScript. Exiting..." 
		exit
	}
}
#=============================================
# Function:WriteLog
# Arguments: LogType, MessageData, $ScriptPath
#=============================================
# Purpose: Appends the passed MessageData to the log type 
# specified
# EXAMPLE WriteLog "ERROR" "Log Message"
#=============================================
Function WriteLog ( $LogType, $MsgData)
{
	$MyCurrentDT = Get-Date
	# Determine Log Type: Error, Debug, Result
	if ($LogType -eq "RESULT") 
	{
		$LogFile = $ScriptPath + "\Log_" + $scriptname + "_Results.csv"
		$MsgData = $MsgData
		$MsgData | out-file -filepath $LogFile -encoding "ASCII" -Append
	}
	elseif ($LogType -eq "ERROR") 
	{
		$LogFile = $ScriptPath + "\Log_" + $scriptname + "_Error.txt"
		$MsgData = $MsgData + "," + $MyCurrentDT
		$MsgData | out-file -filepath $LogFile -encoding "ASCII" -Append
	}
	elseif ($LogType -eq "DEBUG") 
	{
		# Check Debug Toggle
		if ($DebugLogToggle -eq "ON") 
		{
			$LogFile = $ScriptPath + "\Log_" + $scriptname + "_Debug.txt"
			$MsgData = $MsgData + "," + $MyCurrentDT
			$MsgData | out-file -filepath $LogFile -encoding "ASCII" -Append
		}
	}	
}
#=============================================
# Function:EndScript
# Arguments: None
#=============================================
# Purpose: Runs any script ending logic
#=============================================
Function EndScript
{
	$CurrentDT = Get-Date
	$ErrorLogEnd = $scriptname + " script Run: End " + $CurrentDT
	"******************************************"
	"Review log files for script run results."
	"******************************************"
	$scriptname + " script Run: End " + $CurrentDT
	WriteLog "ERROR"  $ErrorLogEnd
}

# Write the ImportStatusCollection to a file
Function SaveCollection
{

	writelog "DEBUG" "SaveCollection,INFO,Saving"
	$Collection | export-csv $DataFilePath -notype

}

function CreateGroupCollection
{
$SQL = Import-Csv ilo_login.csv | where {$_.Safe -like "win*" -and $_.Address -like "*vaswsql100*" -or $_.Address -like "*Vaswsql200*"}
$Blades = Import-Csv ilo_login.csv | where {$_.Safe -like "win*" -and $_.Address -like "*vasfhpc*"}
$ESX = Import-Csv ilo_login.csv | where {$_.Safe -like "win*" -and $_.Address -like "*vasevmw*"}
$Remaining = Import-Csv ilo_login.csv | where {$_.Safe -like "win*" -and ($_.Address -like "vas*" -or $_.Address -like "was*") -and $_.Address -notlike "*vasevmw*" -and $_.Address -notlike "*vaswsql100*" -and $_.Address -notlike "*vaswsql200*" -and $_.Address -notlike "*vasfhpc*"}
$script:servertype = @($SQL,$Blades,$ESX,$Remaining)
}

Function PoweriLO 
{
      $Flash=cmd /c $UTIL -s $ilo -l .\Logs\$ilo.Log -u $usr -p $pass -f $PWRXML
      $CHK1=$Flash | Select-String -Pattern "Script Failed" , "not valid" -CaseSensitive
          If($CHK1 -eq $null) {Write-host "Power ON of iLO: $iLO Succeeded, Proceeding with List" -ForegroundColor Green}
          If($CHK1 -ne $null) {Write-host "Power ON iLO: $iLO Failed, Please Review: `n.\Logs\$ilo.Log for details " -ForegroundColor Magenta;return $CHK1}
         }

Function ProcessILO 
{

    # Check if iLO is active on Network #
    if(!(Test-Connection -Cn $iLO -BufferSize 16 -Count 1 -ea 0 -quiet))
    {
        Write-Host "Unable to PING iLO : $ilo" -ForegroundColor Magenta
        $ilo,$ver,$Date,"No_PING" -join ','|out-file $File -Append;Return
    }

        # Check if iLO URL is Active #
    $Result=Get-WebURL -Url https://$iLO
    If($Result.Status -ne "TrustFailure") 
    {
        Write-Host "Unable to Connect to iLO : $ilo" -ForegroundColor Yellow
        $ilo,$ver,$Date,"Jacked_URL" -join ','|out-file $File -Append;Return
    }

    # Check if iLO is version 3 or 4 if not change to CPQ executable #
    $UTIL="HPQLOCFG.exe"
    $FW=cmd /c $UTIL -s $ilo -u $usr -p "$pass" -f Get_FW_Version.xml
    $CHK0=$FW | Select-String -Pattern "ERROR:HPQLOCFG.exe" -CaseSensitive
    If($CHK0 -ne $null) 
    {
        $UTIL="cpqlocfg.exe"
        $FW=cmd /c $UTIL -s $ilo -u $usr -p "$pass" -f Get_FW_Version.xml
    }

    [String]$Output=get-content $iLO | Select-String -pattern "FIRMWARE", "MANAGEMENT"

    # Check if utility can connect to the iLO #
    If ($Output -eq $null) 
    {
        Write-Host "Unable to Query iLO : $ilo" -ForegroundColor Yellow
        $ilo,$ver,$Date,"UnKnown" -join ','|out-file $File -Append;Return
    }

    # If utility can connect to the iLO Split the output to readable variables #
    $VAL = $output.Split('"(*)"')
    $Ver = $VAL[1]
    $Date = $Val[3]
    $Type = $Val[5]

    write-host "iLO Name:" $ilo, "- Firmware Version:" $ver , "- Date:"$Date , "- iLO Type:" $Type -ForegroundColor Cyan
   
    # Write information to Speadsheet #
    $ilo,$ver,$Date,$Type -join ','|out-file $File -Append

    # Begin of Server Power On #

    $PWRXML = ".\Config\Set_Host_Power_on.xml" ## Server "Power ON" Option File

    IF ($Type -eq "iLO4") 
    {
        powerilo $PWRXML
        If($CHK1 -ne $null){Return}
    }
    IF ($Type -eq "iLO3") 
    {
        powerilo $PWRXML
        If($CHK1 -ne $null){Return}
    }
    IF ($Type -eq "iLO2") 
    {            
        powerilo $PWRXML
        If($CHK1 -ne $null){Return}
    }
    IF ($Type -eq "iLO") 
    {            
        powerilo $PWRXML
        If($CHK1 -ne $null){Return}
    }
            
}

Function RefreshCache
{
    $App=cmd /c $Path2\AppPrvmgr.exe RefreshCache
    If($LASTEXITCODE -eq 0){Write-host "Cyber-Ark Cache refreshed Successfully" -ForegroundColor Green}
    Else{Write-host "Cyber-Ark Cache refresh FAILED - Please check command and try again." -ForegroundColor Red;Break}
}

Function ParseILO
{
#  Creates Base CSV file for iLO Documentation #
    "iLO Name,Firmware Version,Date,iLO Type, iLO4/2.03,iLO3/1.80,iLO2/2.25,iLO/1.96" | out-file $File

# Checks for and/or Creates Logs directory if needed #
    if (-not (Test-Path ".\Logs\"))
    {
        New-Item -Path ".\Logs\" -Type Directory | Out-Null
    }

#  Get List of iLO's to check #
    foreach ($Login in $ServerType)
    {
    #  Set iLO name and User Credentials #
        foreach ($Name in $Login) 
        {
            $Safe=$Name.Safe
            $Caff=$Name.CAFFileName
            $usr=$Name.UserName
            $ilo=$Name.Address
            $Pass=(cmd /c $Path\CLIPasswordSDK.exe GetPassword /p AppDescs.AppID=iLo-ALL /p "Query=Safe=$Safe;Folder=Root;Object=$Caff" /o Password)

    # Escape any special charaters in Password so they will work #
            $Pass = $Pass -replace '\|','^|'

            ProcessILO $ilO

    # Delete the TEMP file created for iLO query #
            If (Test-Path $iLO)
            {
    	        Remove-Item $iLO
            } 
        }
    }
    Write-Host `t""
    Write-Host `t""
    Write-Host `t""
    Write-Host `t""
    Write-Host `t""
    Write-Host `t""
    Write-host "Complete.  Please review console screen and logs for any issues" -ForegroundColor Cyan
}

#*=============================================
#* SCRIPT BODY
#*=============================================
$script:ScriptPath  = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $script:ScriptPath
$script:scriptname = "PowerUp"
$script:DebugLogToggle = "ON"
$FN="Powerup_Log_"
$File=".\$FN$(get-date -Format MM-dd-yyyy_HH.mm).csv"
$Path="C:\Program Files (x86)\CyberArk\ApplicationPasswordSdk"
$Path2="C:\Program Files (x86)\CyberArk\ApplicationPasswordProvider"

# This the path and file name where the output data is saved.
$script:DataFilePath = $ScriptPath + "\VCenterOutput.csv"

# AddPSSnapin for PowerCLI

$error.clear()
InitScript
CreateGroupCollection
RefreshCache
ParseILO
EndScript 
#*=============================================
#* END OF SCRIPT:
#*=============================================
