Param(
  [Parameter(Mandatory=$True,Position=0,ParameterSetName="UserName")]
  [alias("user")]
  $script:user
)

Function InitScript {
	Set-StrictMode -version Latest
    Import-Module ActiveDirectory
    Import-Module BitsTransfer
    $script:user = Get-ADUser $script:user -Properties *
    $script:Paths = @(
                    "\\atlres101p\S02REG101DOCS$\",
                    "\\bosres101p\S03REG101DOCS$\",
                    "\\chires101p\S04REG101DOCS$\",
                    "\\dalwres101p\S05REG101DOCS$\",
                    "\\kcmres101p\S06REG101DOCS$\",
                    "\\memres101p\S07REG101DOCS$\",
                    "\\nycres101p\S08REG101DOCS$\",
                    "\\sfcres101p\S09REG101DOCS$\",
                    "\\WASRES103DOCS\WASRES103DOCS$\",
                    "\\WASOIG103P\WASOIG103Docs$\",
                    "\\VASWRES003P\MyDocs$\",
                    "\\VASWRES004P\MyDocs$\",
                    "\\VASWRES005P\MyDocs$\"
                    )
    $script:MyDocsGroups = @(
                            "UsersS02REG101",
                            "UsersS03REG101",
                            "UsersS04REG101",
                            "UsersS05REG101",
                            "UsersS06REG101",
                            "UsersS07REG101",
                            "UsersS08REG101",
                            "UsersS09REG101",
                            "UsersWASRES103",
                            "R01OIG",
                            "MyDocsVASWRES003P",
                            "MyDocsVASWRES004P",
                            "MyDocsVASWRES005P"
                            )
    $script:SourceDocs = $null
}

Function Test-ADGroupMember {
    Param ($Group)
    If (Get-ADUser -Filter "memberOf -RecursiveMatch '$((Get-ADGroup $Group).DistinguishedName)'" -SearchBase $((Get-ADUser $script:User).DistinguishedName)) {$true}
    Else {$false}
}

Function FindMyDocsGroup {
#Determine current MyDocs NT group
    $script:location = $null
    foreach ($MyDocsgroup in $MyDocsGroups) {
    if ((Test-ADGroupMember $MyDocsgroup) -eq $true)
        {
        $script:location = $MyDocsgroup
        ReportMyDocs
        break
        }
    }
    if ($script:location -eq $null) {Write-Host $script:user.Name "does not have a valid MyDocs redirect!"}
}

Function ReportMyDocs {
#report MyDocs location according to NT group

    Switch ($location) {
        UsersS02REG101 {(($user.name) + "'s MyDocs redirect located at \\atlres101p\S02REG101DOCS$\" + ($user.samaccountname));$script:server = "atlres101p";$script:dest = "\\atlres101p\S02REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS03REG101 {(($user.name) + "'s MyDocs redirect located at \\bosres101p\S03REG101DOCS$\" + ($user.samaccountname));$script:server = "bosres101p";$script:dest = "\\bosres101p\S03REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS04REG101 {(($user.name) + "'s MyDocs redirect located at \\chires101p\S04REG101DOCS$\" + ($user.samaccountname));$script:server = "chires101p";$script:dest = "\\chires101p\S04REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS05REG101 {(($user.name) + "'s MyDocs redirect located at \\dalwres101p\S05REG101DOCS$\" + ($user.samaccountname));$script:server = "dalwres101p";$script:dest = "\\dalwres101p\S05REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS06REG101 {(($user.name) + "'s MyDocs redirect located at \\kcmres101p\S06REG101DOCS$\" + ($user.samaccountname));$script:server = "kcmres101p";$script:dest = "\\kcmres101p\S06REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS07REG101 {(($user.name) + "'s MyDocs redirect located at \\memres101p\S07REG101DOCS$\" + ($user.samaccountname));$script:server = "memres101p";$script:dest = "\\memres101p\S07REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS08REG101 {(($user.name) + "'s MyDocs redirect located at \\nycres101p\S08REG101DOCS$\" + ($user.samaccountname));$script:server = "nycres101p";$script:dest = "\\nycres101p\S08REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersS09REG101 {(($user.name) + "'s MyDocs redirect located at \\sfcres101p\S09REG101DOCS$\" + ($user.samaccountname));$script:server = "sfcres101p";$script:dest = "\\sfcres101p\S09REG101DOCS$\" + ($user.samaccountname) ; Continue}
        UsersWASRES103 {(($user.name) + "'s MyDocs redirect located at \\WASRES103DOCS\WASRES103DOCS$\" + ($user.samaccountname));$script:server = "WASRES103DOCS";$script:dest = "\\WASRES103DOCS\WASRES103DOCS$\" + ($user.samaccountname) ; Continue}
        R01OIG {(($user.name) + "'s MyDocs redirect located at \\WASOIG103P\WASOIG103Docs$\" + ($user.samaccountname));$script:server = "WASOIG103P";$script:dest = "\\WASOIG103P\WASOIG103Docs$\" + ($user.samaccountname) ; Continue}
        MyDocsVASWRES003P {(($user.name) + "'s MyDocs redirect located at \\VASWRES003P\MyDocs$\" + ($user.samaccountname));$script:server = "VASWRES003P";$script:dest = "\\VASWRES003P\MyDocs$\" + ($user.samaccountname) ; Continue}
        MyDocsVASWRES004P {(($user.name) + "'s MyDocs redirect located at \\VASWRES004P\MyDocs$\" + ($user.samaccountname));$script:server = "VASWRES004P";$script:dest = "\\VASWRES004P\MyDocs$\" + ($user.samaccountname) ; Continue}
        MyDocsVASWRES005P {(($user.name) + "'s MyDocs redirect located at \\VASWRES005P\MyDocs$\" + ($user.samaccountname));$script:server = "VASWRES005P";$script:dest = "\\VASWRES005P\MyDocs$\" + ($user.samaccountname) ; Continue}
        default {Write-Host "Seemingly no MyDocs security group." ; break}
    }
}

Function FindOldMyDocs {
#We have to search for original path, it isn't provided.
    foreach ($path in $Paths) {
        $path = $path+$user.SamAccountName
        #Write-Host "checking " $path
        if ((Test-Path $path) -eq $true -and $path -notlike "*$server*") {
            Write-Host -ForegroundColor Green "Additional MyDocs path exists!"`n$path
            $script:SourceDocs = $path
            TransferData
        }
    }
}

Function TransferData {
#Use Copy-Item to transfer folder structure to new location
    $source = $SourceDocs
    $dest = $dest + "\oldmydocs"
    Write-Host "Start transfer from $source to $dest"
    Get-ChildItem -Path $source -Recurse | Copy-Item -Destination {
        if ($_.PSIsContainer) {
            Join-Path $dest $_.Parent.FullName.Substring($source.length)
        } else {
            Join-Path $dest $_.FullName.Substring($source.length)
        }
    } -Force
    Write-Host "Transfer Complete"
}

#####
#Main body of script
#####
InitScript
FindMyDocsGroup
Write-Host (($script:user.name) + "'s Home Drive located at " + ($script:user.HomeDirectory))
FindOldMyDocs
if ($script:SourceDocs -ne $null) { TransferData }